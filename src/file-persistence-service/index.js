const Kafka = require('node-rdkafka');
const saveToGoogleDrive = require('./src/googledrive');
const logger = require('./src/logger')('file-persistence-service');

const consumer = new Kafka.KafkaConsumer({
  'group.id': 'file-persistence-cg',
  'metadata.broker.list': 'kafka',
}, {});


consumer.connect();
consumer.on('event.error', function (err) {
  logger.error('Error from producer');
  logger.error(err);
})
consumer
  .on('ready', function () {
    logger.info('subscribing to file-generated topic');
    consumer.subscribe(['file-generated']);
    consumer.consume();
  })
  .on('data', async function (data) {
    const { filename, generatedFile, year } = JSON.parse(data.value.toString());

    try{
      const response = await saveToGoogleDrive(year, filename, generatedFile);
      logger.info(`file saved to google drive with name: ${filename} for year ${year}`);
      logger.info(`response is ${response}`);
    } catch(err){
      logger.error(err);
    }
  })
  .on('error', (err) => {
    logger.error(err);
  });
