const { google } = require('googleapis');
const async = require('async');

const { GAPI_CLIENT_ID,
  GAPI_CLIENT_SECRET,
  GAPI_ACCESS_TOKEN,
  GAPI_REFRESH_TOKEN } = process.env;

const saveToGoogleDrive = async (year, filename, filecontent) => {
  const oauth2Client = new google.auth.OAuth2(
    GAPI_CLIENT_ID,
    GAPI_CLIENT_SECRET,
    'urn:ietf:wg:oauth:2.0:oob'
  );

  google.options({
    auth: oauth2Client
  });
  oauth2Client.setCredentials({
    access_token: GAPI_ACCESS_TOKEN,
    refresh_token: GAPI_REFRESH_TOKEN,
    scope: 'https://www.googleapis.com/auth/drive',
    token_type: 'Bearer',
    expiry_date: 1580427810539
  });

  oauth2Client.on('tokens', (tokens) => {

  });

  const drive = google.drive({
    version: 'v3',
    auth: oauth2Client
  });

  const foldersList = await drive.files.list({
    q: "mimeType='application/vnd.google-apps.folder'",
    fields: 'nextPageToken, files(id, name)',
    pageSize: 1000
  });
  const yearFolder = foldersList.data.files.filter(folderData => {
    return folderData.name == year;
  });

  if (!yearFolder || !yearFolder[0]) {
    throw new Error(`year folder is missing for ${year} ${yearFolder}`);
  }

  var fileMetadata = {
    'name': filename,
    parents: [yearFolder[0].id]
  };

  const res = await drive.files.create({
    resource: fileMetadata,
    media: {
      mimeType: 'text/plain',
      body: filecontent
    },
  });
  if (res.status === 200) {
    return true;
  }
  throw new Error(res);
}

module.exports = saveToGoogleDrive;