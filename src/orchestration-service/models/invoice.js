module.exports = (sequelize, DataTypes) => {
  const Invoice = sequelize.define('Invoice', {
    NIP: DataTypes.BIGINT,
    issuanceDate: DataTypes.DATE,
    costs: DataTypes.NUMBER,
  }, {});
  Invoice.associate = (models) => {
    // models.Invoice.hasMany(models.InvoiceRow);
  };
  return Invoice;
};
