module.exports = (sequelize, DataTypes) => {
  const User = sequelize.define('Users', {
    firstName: DataTypes.STRING,
    NIP: DataTypes.BIGINT,
    email: DataTypes.STRING,
    password: DataTypes.STRING,
  }, {});
  return User;
};
