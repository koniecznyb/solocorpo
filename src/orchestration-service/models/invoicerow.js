module.exports = (sequelize, DataTypes) => {
  const InvoiceRow = sequelize.define('InvoiceRow', {
    description: DataTypes.STRING,
    numer: DataTypes.INTEGER,
    costs: DataTypes.NUMBER,
    quantity: DataTypes.NUMBER,
  }, {});
  InvoiceRow.associate = (models) => {
    // models.InvoiceRow.hasOne(models.Invoice);
  };
  return InvoiceRow;
};
