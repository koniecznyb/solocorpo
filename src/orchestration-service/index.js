const app = require('express')();
const http = require('http');
const helmet = require('helmet');
const bodyParser = require('body-parser');
const shortid = require('shortid');
const httpContext = require('express-http-context');
const routes = require('./src/routes');
const securityService = require('./src/security/securityMiddleware');
const logger = require('./src/logger')('index');
const cookieParser = require('cookie-parser');

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(cookieParser());
app.use(helmet());
app.use(securityService.authenticate);
app.use((req, res, next) => {
  httpContext.set('reqId', shortid.generate());
  next();
});
app.use(httpContext.middleware);
app.use('/', routes);


const serverStartup = () => {
  http.createServer(app).listen(8081, '0.0.0.0');
  logger.info(`server listening on ${'0.0.0.0'}:${8081}`);
};

serverStartup();
