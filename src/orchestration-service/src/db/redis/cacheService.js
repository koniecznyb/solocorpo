const redis = require('redis');

const client = redis.createClient(6379, 'redis');
const { promisify } = require('util');
const logger = require('../../logger')('cacheService');

const getAsync = promisify(client.get).bind(client);

client.on('error', (err) => {
  logger.error(`Error ${err}`);
});

client.on('ready', () => {
  logger.info('connection to redis established');
});

const cachePut = (key, value) => {
  logger.info(`caching key ${key} value ${value}`);
  client.set(key, value);
};

const cacheGet = async (key) => {
  return await getAsync(key);
};


module.exports = {
  cachePut, cacheGet,
};
