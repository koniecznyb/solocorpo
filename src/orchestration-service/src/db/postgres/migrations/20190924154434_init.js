
exports.up = (knex) => knex.schema
  .createTable('company', (table) => {
    table.increments('id');
    table.string('vatNumber', 20).notNullable();
    table.string('name', 100).notNullable();
    table.string('address', 100).notNullable();
    table.string('country', 10).notNullable();
  })
  .createTable('invoice', (table) => {
    table.increments('id');
    table.string('description', 100).notNullable();
    table.date('issuanceDate').notNullable();
    table.string('number', 100).notNullable();
    table.biginteger('net').notNullable();
    table.biginteger('vat');
    table.biginteger('gross').notNullable();
    table.integer('company').unsigned().notNullable();
    table.foreign('company').references('id').inTable('company');
  })
  .createTable('user', (table) => {
    table.increments('id');
    table.string('email').notNullable();
    table.string('name', 100).notNullable();
    table.string('password', 100).notNullable();
  });
exports.down = (knex) => knex.schema
  .dropTable('invoice')
  .dropTable('company')
  .dropTable('user');
