const dbConfig = {
  database: process.env.POSTGRES_NAME,
  username: process.env.POSTGRES_USER,
  password: process.env.POSTGRES_PASSWORD,
  host: 'sqldb',
  dialect: 'postgres',
};

module.exports = {
  development: dbConfig,
  production: dbConfig,
};
