const { POSTGRES_USER, POSTGRES_PASSWORD } = process.env;
const Sequelize = require('sequelize');

const sequelize = new Sequelize(`postgres://${POSTGRES_USER}:${POSTGRES_PASSWORD}@sqldb:5432/${POSTGRES_USER}`);


module.exports = { sequelize, Sequelize };
