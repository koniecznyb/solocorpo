const { convertArrayToCSV } = require('convert-array-to-csv');
const routes = require('express').Router();
const httpService = require('./services/httpService');
const logger = require('./logger')('routes');
const authenticationService = require('./security/authenticationService');
const userService = require('./security/userService');
const kafkaService = require('./services/kafkaService');

const handle500 = (err, res) => {
  logger.error(err);
  res.status(500).send(`Technical problem, try again letter.`);
};

routes.post('/authenticate', async (req, res) => {
  const { name, pass } = authenticationService.extractCredentials(req);
  const authenticationResult = await authenticationService.authenticate(name, pass);
  if (!authenticationResult.ok) {
    logger.error(authenticationResult.error);
    res.status(401).send(authenticationResult.error);
    return;
  }
  const securityToken = authenticationService.signJWTToken(name);
  const securityHeader = `Max-Age=2600000; Domain=${process.env.PROXY_HOST || 'localhost'}; Path=/; SameSite=Strict; HttpOnly`
  res.setHeader('Content-Type', 'application/json')
  res.setHeader('Set-Cookie', [`Authorization=Bearer ${securityToken}; ${securityHeader}`,
  `RefreshToken=test; ${securityHeader}`]);
  res.status(200).send({ user: name });
});

routes.post('/customer/', async (req, res) => {
  try {
    const user = await userService.createNewUser(req);
    res.set('Content-Type', 'application/xml');
    res.send(user);
  } catch (err) {
    handle500(err, res);
  }

});

const handlePostRouteAction = (req, res, action) => {
  const { body } = req;
  if (!body) {
    res.status(400).send('body is mandatory');
  }
  try {
    action(body);
    res.status(200).end();
  } catch (err) {
    handle500(err, res);
  }
}
routes.post('/generateVATUE', (req, res) => {
  handlePostRouteAction(req, res, (requestBody) => kafkaService.publishFileGenerationMessage(requestBody, 'VATUE'));
});

routes.post('/generateJPKVAT', (req, res) => {
  handlePostRouteAction(req, res, (requestBody) => kafkaService.publishFileGenerationMessage(requestBody, 'JPKVAT'));
});

routes.get('/vat/:country/:vatNumber', async (req, res) => {
  try {
    const fetchedData = await httpService.fetchCompanyData(req.params.country, req.params.vatNumber);
    res.set('Content-Type', 'application/json');
    res.send(fetchedData);
  } catch (err) {
    handle500(err, res);
  }
});

routes.get('/currency/:currency/:date', async (req, res) => {
  const currency = req.params.currency;
  const date = req.params.date;
  try {
    const response = await httpService.fetchNBPCurrencyRate(currency, date);
    res.set('Content-Type', 'application/json');
    res.send(response);
  } catch (err) {
    handle500(err, res);
  }
});

const parseKPIR = (kpirdata) => {
  const headers = [2, 3, 4, 5, 6, 7, 13];
  return convertArrayToCSV(kpirdata, {
    headers,
    separator: ';',
  });
};

routes.post('/kpir', (req, res) => {
  const { body } = req;

  if (!body) {
    res.status(400).send('body is mandatory');
  }
  try {
    const response = parseKPIR(body);
    logger.info(`response is ${response}`);
    res.set('Content-Type', 'application/xml');
    res.send(response);
  } catch (err) {
    handle500(err, res);
  }
});

module.exports = routes;
