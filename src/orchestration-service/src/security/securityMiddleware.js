const logger = require('../logger')('securityMiddleware');
const authenticationService = require('./authenticationService');

const authenticate = (req, res, next) => {
  if ( ['/authenticate', '/customer'].includes(req.url) ) {
    next();
    return;
  }

  const authCookie = req.cookies['Authorization'];
  if (!authCookie) {
    const msg = 'missing authorization cookie';
    logger.error(msg);
    res.status(401).send(msg);
    return;
  }

  if (!authCookie.startsWith('Bearer ')) {
    const msg = 'invalid format of authorization token';
    logger.error(msg);
    res.status(401).send(msg);
    return;
  }

  const isAuthenticated = authenticationService.verifyJWTToken(
    authCookie.substring(7, authCookie.length),
  );
  if (!isAuthenticated) {
    const msg = 'users token validation failed';
    logger.error(msg);
    res.status(401).send(msg);
    return;
  }
  logger.info(`user authorized for ${req.url}`);
  next();
};


module.exports.authenticate = authenticate;
