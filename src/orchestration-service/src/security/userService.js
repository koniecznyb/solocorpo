const logger = require('../logger')('userService');
const DB = require('../../models/index');
const bcrypt = require('bcryptjs');

const createNewUser = async (req) => {
  const { body } = req;
  if (!body) {
    throw new Error('missing body');
  }

  const generateHash = async (password) => await bcrypt.hash(password, bcrypt.genSaltSync(8));
  const { firstName, password, NIP, email } = body;
  const hashedPassword = await generateHash(password);
  return await DB.Users.create({ firstName, NIP, email, password: hashedPassword });
};

module.exports.createNewUser = createNewUser;