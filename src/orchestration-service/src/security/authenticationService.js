// const Sequelize = require('sequelize');
const fs = require('fs');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');
const auth = require('basic-auth');
const logger = require('../logger')('authenticationService');
const DB = require('../../models/index');

const privateKEY = fs.readFileSync('security/private-dev.key', 'utf8');
const publicKEY = fs.readFileSync('security/public-dev.key', 'utf8');

const signOptions = {
  issuer: 'koniecznyb',
  subject: 'some@user.com',
  expiresIn: '12h',
  audience: 'some@user.com',
  algorithm: 'RS256',
};

const authenticate = async (name, pass) => {
  const user = await DB.Users.findOne({ where: { firstName: name } });
  if (!user) {
    return {
      ok: false,
      error: `user ${name} was not found`,
    };
  }
  const validatePassword = async (firstPassword, secondPassword) => bcrypt.compare(firstPassword, secondPassword);
  const isValidPassword = await validatePassword(pass, user.password);
  if (!isValidPassword) {
    return {
      ok: false,
      error: `invalid password for user ${name}`,
    };
  }
  logger.info(`User [${name}] succesfully authenticated with password`);
  return {
    ok: true,
  };
};

const signJWTToken = (username) => {
  // signOptions.audience = username;
  // signOptions.subject = `${username}@mail.com`;
  const token = jwt.sign({ username }, privateKEY, signOptions);
  logger.info(`Token - ${token}`);
  return token;
};

const verifyJWTToken = (token) => {
  const verifyOptions = {
    issuer: 'koniecznyb',
    subject: 'some@user.com',
    expiresIn: '12h',
    audience: 'some@user.com',
    algorithm: 'RS256',
  };
  try{
    return jwt.verify(token, publicKEY, verifyOptions);
  } catch (err){
    logger.error(`JWT verification failed`);
    return false;
  }
};

const extractCredentials = (req) => {
  const credentials = auth(req);
  if (!credentials) {
    throw new Error('missing credentials');
  }
  return credentials;
};

module.exports.verifyJWTToken = verifyJWTToken;
module.exports.signJWTToken = signJWTToken;
module.exports.authenticate = authenticate;
module.exports.extractCredentials = extractCredentials;