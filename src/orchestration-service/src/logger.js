const { createLogger, transports } = require('winston');
const httpContext = require('express-http-context');
const { format } = require('logform');
const winston = require('winston')

const getRequestId = () => {
  const reqId = httpContext.get('reqId');
  return reqId ? `[${reqId}]` : '';
};

const alignedWithColorsAndTime = (moduleName) => format.combine(
  format.timestamp(),
  format.ms(),
  format.printf((info) => `[${info.timestamp}][${moduleName}]${getRequestId()} ${info.level}: ${info.message}`),
);

const options = {
  filename: 'orchestration-service.log',
};

const logger = (moduleName) => createLogger({
  format: alignedWithColorsAndTime(moduleName),
  transports: [
    new transports.Console(),
    new winston.transports.File(options),
  ],
  
});

module.exports = (moduleName) => logger(moduleName);
