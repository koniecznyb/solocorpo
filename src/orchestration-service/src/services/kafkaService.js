const kafka = require('node-rdkafka');
const logger = require('../logger')('kafkaService');

const producer = new kafka.Producer({
    'metadata.broker.list': 'kafka',
    'dr_cb': true,
});

producer.connect();
producer.on('event.error', (err) => {
    logger.error(`Error from producer: ${err}`);
});

const publishFileGenerationMessage = (requestBody, messageType) => {
    const content = Buffer.from(JSON.stringify({ ...requestBody, messageType }));
    const topic = 'file-generation-request';
    producer.produce(
        topic,
        null,
        content,
        null,
        Date.now(),
    );
    logger.info(`published a message to topic [${topic}] for file type ${messageType}`);
}

module.exports.publishFileGenerationMessage = publishFileGenerationMessage;