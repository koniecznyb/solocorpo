const fetch = require('node-fetch');
const { cachePut, cacheGet } = require('../db/redis/cacheService');
const logger = require('../logger')('httpService');

const fetchCompanyData = async (countryArg, vatNumber) => {
    const country = countryArg.toUpperCase();
    if (!country || !vatNumber) {
        throw new Error(`Invalid request data ${country}, ${vatNumber}`);
    }

    const cached = await cacheGet(country + vatNumber);
    if (cached) {
        logger.info(`found a cached response ${cached} for vat number [${vatNumber}] and country [${country}]`);
        return cached;
    }
    const response = await fetch(`http://nginx-proxy:8079/company-data-service/vat/${country}/${vatNumber}`);
    const data = await response.json();
    if (!data) {
        throw new Error('response data is empty');
    }
    cachePut(country + vatNumber, JSON.stringify(data));
    return data;
};

const fetchNBPCurrencyRate = async (currency, date) => {
    currency = currency.toUpperCase();
    if (!currency || !date) {
        throw new Error(`Invalid request data ${currency}, ${date}`);
    }

    const cached = await cacheGet(currency + date);
    if (cached) {
        logger.info(`found a cached response ${cached} for currency [${currency}] and date [${date}]`);
        return cached;
    }

    logger.info(`getting exchange rate for currency ${currency} and date ${date}`);
    const url = "http://api.nbp.pl/api/exchangerates/rates/a/" + currency + "/" + date + "/?format=json";
    logger.info(`fetching url ${url}`);
    const response = await fetch(url);
    const responseContent = await response.json();
    const rate = responseContent.rates[0].mid;

    logger.info(`response from NBP rate: ${rate}, currency: ${currency}, date: ${date}`);

    if (rate == null) {
        logger.error(`invalid response: ${responseContent}`);
        throw new Error();
    }
    const responseObject = { rate, currency, date };
    cachePut(currency + date, JSON.stringify(responseObject));
    return { ...responseObject };
}

module.exports.fetchCompanyData = fetchCompanyData;
module.exports.fetchNBPCurrencyRate = fetchNBPCurrencyRate;
