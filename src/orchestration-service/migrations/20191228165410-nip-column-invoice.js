module.exports = {
  async up(queryInterface, Sequelize) {
    const transaction = await queryInterface.sequelize.transaction();
    try {
      await queryInterface.removeColumn('Invoices', 'nip', { transaction });
      await queryInterface.addColumn('Invoices', 'NIP', { type: Sequelize.BIGINT }, { transaction });
      await transaction.commit();
    } catch (err) {
      await transaction.rollback();
      throw err;
    }
  },
  async down(queryInterface, Sequelize) {
    const transaction = await queryInterface.sequelize.transaction();
    try {
      await queryInterface.removeColumn('Invoices', 'NIP', { transaction });
      await queryInterface.addColumn('Invoices', 'nip', { type: Sequelize.INTEGER }, { transaction });
      await transaction.commit();
    } catch (err) {
      await transaction.rollback();
      throw err;
    }
  },
};
