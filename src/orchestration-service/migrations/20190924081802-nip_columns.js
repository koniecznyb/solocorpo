module.exports = {
  async up(queryInterface, Sequelize) {
    const transaction = await queryInterface.sequelize.transaction();
    try {
      await queryInterface.addColumn('Invoices', 'nip', { type: Sequelize.INTEGER }, { transaction });
      await queryInterface.removeColumn('Invoices', 'fromNIP', { transaction });
      await queryInterface.removeColumn('Invoices', 'toNIP', { transaction });
      await transaction.commit();
    } catch (err) {
      await transaction.rollback();
      throw err;
    }
  },
  async down(queryInterface, Sequelize) {
    const transaction = await queryInterface.sequelize.transaction();
    try {
      await queryInterface.addColumn('Invoices', 'fromNIP', { type: Sequelize.INTEGER }, { transaction });
      await queryInterface.addColumn('Invoices', 'toNIP', { type: Sequelize.INTEGER }, { transaction });
      await queryInterface.removeColumn('Invoices', 'nip', { transaction });
      await transaction.commit();
    } catch (err) {
      await transaction.rollback();
      throw err;
    }
  },
};
