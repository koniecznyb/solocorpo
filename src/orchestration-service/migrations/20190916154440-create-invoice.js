module.exports = {
  up: (queryInterface, Sequelize) => queryInterface.createTable('Invoices', {
    id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: Sequelize.INTEGER,
    },
    fromNIP: {
      type: Sequelize.INTEGER,
    },
    toNIP: {
      type: Sequelize.INTEGER,
    },
    issuanceDate: {
      type: Sequelize.DATE,
    },
    costs: {
      type: Sequelize.BIGINT,
    },
    createdAt: {
      allowNull: false,
      type: Sequelize.DATE,
    },
    updatedAt: {
      allowNull: false,
      type: Sequelize.DATE,
    },
  }),
  down: (queryInterface) => queryInterface.dropTable('Invoices'),
};
