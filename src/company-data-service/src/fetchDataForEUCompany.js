const fetch = require('node-fetch');
const { parseString } = require('xml2js');
const { promisify } = require('util');
const logger = require('./logger')('fetchDataForEUCompany');

const parseStringAsync = promisify(parseString);
const url = 'http://nginx-proxy:8080/external/findEUVat/';

const handleError = (message, error) => {
  logger.info(message);
  throw error;
};

const squeeze = (string) => string.replace(/\s+/g, ' ');
const fetchDataForEUCompany = async (vatNumber, country) => {
  const requestBody = `<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:pm="urn:ec.europa.eu:taxud:vies:services:checkVat:types">
  <soapenv:Header></soapenv:Header>
  <soapenv:Body>
    <pm:checkVat>
     <pm:countryCode>${country}</pm:countryCode>
     <pm:vatNumber>${vatNumber}</pm:vatNumber>
    </pm:checkVat>
  </soapenv:Body>
 </soapenv:Envelope>`;

  const extractCompanyData = (json) => {
    const vatResponse = json['soap:Envelope']['soap:Body'][0].checkVatResponse[0];
    const address = vatResponse.address[0];
    const name = vatResponse.name[0];
    return {
      address: squeeze(address),
      name: squeeze(name),
    };
  };


  const response = await fetch(url, {
    method: 'POST',
    body: requestBody,
    headers: {
      'Content-Type': 'text/xml',
    },
  })
    .then((res) => res.text())
    .then(parseStringAsync)
    .then(extractCompanyData)
    .catch((err) => handleError('Error while fetching data', err));

  return response;
};

module.exports = fetchDataForEUCompany;
