const http = require('http');
const bodyParser = require('body-parser');
const app = require('express')();
const logger = require('./src/logger')('company-data-service');
const fetchDataForEUCompany = require('./src/fetchDataForEUCompany');
const fetchDataForPLCompany = require('./src/fetchDataForPLCompany');

const handle500 = (err, res) => {
    logger.error(err);
    res.status(500).send(`Technical problem: ${err}`);
  };

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.get('/vat/:country/:vat', async (req, res) => {
    const vat = req.params.vat.replace(/-/g, '');
    const country = req.params.country.toUpperCase();
    try {
        const data = country === 'PL'
        ? await fetchDataForPLCompany(vat)
        : await fetchDataForEUCompany(vat, country);
        logger.info(`fetched vat number for ${vat} and country ${country}: ${JSON.stringify(data)}`)
        res.send(data);
    } catch (err) {
        handle500(err, res);
    }
});


const serverStartup = () => {
  http.createServer(app).listen(8084, '0.0.0.0');
  logger.info('server startup on port 8084')
};

serverStartup();
