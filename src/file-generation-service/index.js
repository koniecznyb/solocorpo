const Kafka = require('node-rdkafka');
const generateVATUE = require('./src/generateVATUE');
const generateJPKVATFile = require('./src/generateJPK');
const logger = require('./src/logger')('file-generation-service');

const responseTopic = 'file-generated';

const consumer = new Kafka.KafkaConsumer({
  'group.id': 'file-generation-cg',
  'metadata.broker.list': 'kafka',
}, {});

const producer = new Kafka.Producer({
  'metadata.broker.list': 'kafka',
  'dr_cb': true,
});

producer.connect();
producer.on('event.error', function (err) {
  logger.error('Error from producer');
  logger.error(err);
})

const publishFilePersistenceMessage = async (year, generatedFile, filename) => {
  const content = Buffer.from(JSON.stringify({ year, generatedFile, filename }));
  producer.produce(
    responseTopic,
    null,
    content,
    null,
    Date.now(),
  );
};


consumer.connect();
consumer.on('event.error', function (err) {
  logger.error('Error from producer');
  logger.error(err);
  throw err;
})
consumer
  .on('ready', function () {
    const topic = 'file-generation-request';
    logger.info(`subscribing to ${topic}`);
    consumer.subscribe([topic]);
    consumer.consume();
  })
  .on('data', async function (data) {
    const messageContent = JSON.parse(data.value.toString());

    const date = new Date(messageContent.accountingPeriod);
    const year = date.getFullYear();
    const month = date.getMonth()+1;

    const generatedFile = messageContent.messageType === 'VATUE'
      ? generateVATUE({
        year,
        month,
        ...messageContent
      })
      : generateJPKVATFile(messageContent);

    const filename = `${messageContent.messageType}${month}-${year}.xml`;
    logger.info(`generated file: ${generatedFile}`);
    await publishFilePersistenceMessage(year, generatedFile, filename);
    logger.info(`published newly generated file [${filename}] to topic [${responseTopic}]`);
  })
  .on('error', (err) => {
    logger.error(err);
  });
