const xmlbuilder = require('xmlbuilder');

const generateVATUE = ({year, month, counterparty, personalDetails}) => {
  if (!year || !month || !counterparty || !personalDetails) throw new Error('invalid input parameters');

  const {
    nip, name, surname, dateOfBirth, lastPITAmount,
  } = personalDetails;

  const {
    countryCode, vatNumber, amount,
  } = counterparty;

  const vatUE = xmlbuilder.create('Deklaracja', { encoding: 'UTF-8' })
    .att('xmlns', 'http://crd.gov.pl/wzor/2017/01/11/3846/')
    .att('xmlns:etd', 'http://crd.gov.pl/xml/schematy/dziedzinowe/mf/2016/01/25/eD/DefinicjeTypy/')
    .ele('Naglowek')
    .ele('KodFormularza', {
      kodSystemowy: 'VAT-UE (4)',
      wersjaSchemy: '1-0E',
    }, 'VAT-UE')
    .up()
    .ele('WariantFormularza', '4')
    .up()
    .ele('Rok', year)
    .up()
    .ele('Miesiac', month)
    .up()
    .ele('CelZlozenia', '0')
    .up()
    .ele('KodUrzedu', 2413)
    .up()
    .up();

  vatUE.ele('tns:Podmiot1').att('rola', 'Podatnik')
    .ele('etd:OsobaFizyczna')
    .ele('etd:NIP', nip)
    .up()
    .ele('etd:ImiePierwsze', name)
    .up()
    .ele('etd:Nazwisko', surname)
    .up()
    .ele('etd:DataUrodzenia', dateOfBirth)
    .up()
    .up()
    .up();


  vatUE.ele('PozycjeSzczegolowe')
    .ele('Grupa3')
    .ele('P_Ua', countryCode)
    .up()
    .ele('P_Ub', vatNumber)
    .up()
    .ele('P_Uc', amount)
    .up()
    .up()
    .up();

  vatUE.ele('Pouczenie', 1);

  vatUE.ele('podp:DaneAutoryzujace').att('xmlns:podp', 'http://e-deklaracje.mf.gov.pl/Repozytorium/Definicje/Podpis/')
    .ele('podp:NIP', '9691622452').up()
    .ele('podp:ImiePierwsze', 'BARTŁOMIEJ')
    .up()
    .ele('podp:Nazwisko', 'KONIECZNY')
    .up()
    .ele('podp:DataUrodzenia', '1993-01-28')
    .up()
    .ele('podp:Kwota', lastPITAmount)
    .up();

  return vatUE.end({ pretty: true });
};

module.exports = generateVATUE;
