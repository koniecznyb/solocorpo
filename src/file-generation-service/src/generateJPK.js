const _ = require('lodash');
const xmlbuilder = require('xmlbuilder');


const roundUp = (number) => Number(`${Math.round(`${number}e2`)}e-2`);
const formatDate = (date) => new Date(date).toISOString().split('T')[0];

const generateJPKVATFile = ({ expensedProductInvoices, vatIssuedInvoices, expensedCarInvoices, euIssuedInvoices, accountingPeriod }) => {
  let date = new Date(accountingPeriod);
  const firstDay = new Date(date.getFullYear(), date.getMonth(), 1);
  const lastDay = new Date(date.getFullYear(), date.getMonth() + 1, 0);


  const jpk = xmlbuilder
    .create('tns:JPK', { encoding: 'UTF-8' })
    .att('xmlns:tns', 'http://jpk.mf.gov.pl/wzor/2017/11/13/1113/')
    .att('xmlns:etd', 'http://crd.gov.pl/xml/schematy/dziedzinowe/mf/2016/01/25/eD/DefinicjeTypy/')
    .att('xmlns:xsi', 'http://www.w3.org/2001/XMLSchema-instance')
    .ele('tns:Naglowek')
    .ele('tns:KodFormularza', {
      kodSystemowy: 'JPK_VAT (3)',
      wersjaSchemy: '1-1',
    }, 'JPK_VAT')
    .up()
    .ele('tns:WariantFormularza', '3')
    .up()
    .ele('tns:CelZlozenia', '0')
    .up()
    .ele('tns:DataWytworzeniaJPK', new Date().toISOString().split('.')[0])
    .up()
    .ele('tns:DataOd', formatDate(firstDay))
    .up()
    .ele('tns:DataDo', formatDate(lastDay))
    .up()
    .ele('tns:NazwaSystemu', 'excel')
    .up()
    .up();

  jpk.ele('tns:Podmiot1')
    .ele('tns:NIP', '9691622452').up()
    .ele('tns:PelnaNazwa', 'Bartłomiej Konieczny IT Consulting')
    .up()
    .ele('tns:Email', 'konieczny93@gmail.com')
    .up();


  let issuedSum = 0;
  let issuedCount = 0;
  vatIssuedInvoices.forEach((invoice) => {
    if (!invoice.NIP) return;
    jpk.ele('tns:SprzedazWiersz')
      .ele('tns:LpSprzedazy', issuedCount + 1).up()
      .ele('tns:NrKontrahenta', invoice.NIP)
      .up()
      .ele('tns:NazwaKontrahenta', invoice.NAZWA)
      .up()
      .ele('tns:AdresKontrahenta', invoice.ADRES_DOSTAWY)
      .up()
      .ele('tns:DowodSprzedazy', invoice.NR_DOKUMENTU)
      .up()
      .ele('tns:DataWystawienia', formatDate(invoice.DATA_WYSTAWIENIA))
      .up()
      .ele('tns:DataSprzedazy', formatDate(invoice.DATA_WYSTAWIENIA))
      .up()
      .ele('tns:K_19', roundUp(invoice.NETTO))
      .up()
      .ele('tns:K_20', roundUp(invoice.VAT))
      .up();
    issuedSum += roundUp(invoice.VAT);
    issuedCount += 1;
  });


  euIssuedInvoices.forEach((invoice) => {
    if (!invoice.NIP) return;
    jpk.ele('tns:SprzedazWiersz')
      .ele('tns:LpSprzedazy', issuedCount + 1).up()
      .ele('tns:NrKontrahenta', invoice.KRAJ + invoice.NIP)
      .up()
      .ele('tns:NazwaKontrahenta', invoice.NAZWA)
      .up()
      .ele('tns:AdresKontrahenta', invoice.ADRES_DOSTAWY)
      .up()
      .ele('tns:DowodSprzedazy', invoice.NR_DOKUMENTU)
      .up()
      .ele('tns:DataWystawienia', formatDate(invoice.DATA_WYSTAWIENIA))
      .up()
      .ele('tns:DataSprzedazy', formatDate(invoice.DATA_WYSTAWIENIA))
      .up()
      .ele('tns:K_11', roundUp(invoice.W_PLN))
      .up()
      .ele('tns:K_12', roundUp(invoice.W_PLN))
      .up();
    issuedCount += 1;
  });

  jpk.ele('tns:SprzedazCtrl')
    .ele('tns:LiczbaWierszySprzedazy', issuedCount).up()
    .ele('tns:PodatekNalezny', roundUp(issuedSum));

  let expenseSum = 0;
  let expensedCount = 0;
  expensedProductInvoices.forEach((invoice, id) => {
    if (!invoice.NIP) return;
    jpk.ele('tns:ZakupWiersz')
      .ele('tns:LpZakupu', expensedCount + 1).up()
      .ele('tns:NrDostawcy', invoice.NIP)
      .up()
      .ele('tns:NazwaDostawcy', invoice.NAZWA)
      .up()
      .ele('tns:AdresDostawcy', invoice.ADRES_DOSTAWY)
      .up()
      .ele('tns:DowodZakupu', invoice.NR_DOKUMENTU)
      .up()
      .ele('tns:DataZakupu', formatDate(invoice.DATA_WYSTAWIENIA))
      .up()
      .ele('tns:DataWplywu', formatDate(invoice.DATA_WYSTAWIENIA))
      .up()
      .ele('tns:K_45', roundUp(invoice.NETTO))
      .up()
      .ele('tns:K_46', roundUp(invoice.VAT))
      .up();
    expenseSum += invoice.VAT;
    expensedCount += 1;
  });


  expensedCarInvoices.forEach((invoice, id) => {
    if (!invoice.NIP) return;
    jpk.ele('tns:ZakupWiersz')
      .ele('tns:LpZakupu', expensedCount + 1).up()
      .ele('tns:NrDostawcy', invoice.NIP)
      .up()
      .ele('tns:NazwaDostawcy', invoice.NAZWA)
      .up()
      .ele('tns:AdresDostawcy', invoice.ADRES_DOSTAWY)
      .up()
      .ele('tns:DowodZakupu', invoice.NR_DOKUMENTU)
      .up()
      .ele('tns:DataZakupu', formatDate(invoice.DATA_WYSTAWIENIA))
      .up()
      .ele('tns:DataWplywu', formatDate(invoice.DATA_WYSTAWIENIA))
      .up()
      .ele('tns:K_45', roundUp(invoice['50%_NETTO']))
      .up()
      .ele('tns:K_46', roundUp(invoice.VAT))
      .up();
    expenseSum += invoice.VAT;
    expensedCount += 1
  });


  jpk.ele('tns:ZakupCtrl')
    .ele('tns:LiczbaWierszyZakupow', expensedCount).up()
    .ele('tns:PodatekNaliczony', roundUp(expenseSum));

  return jpk.end({ pretty: true });
};

module.exports = generateJPKVATFile;
