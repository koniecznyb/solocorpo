#!/bin/bash

for d in src/*/
do
    ( cd "$d" && docker push "$REGISTRY/solocorpo-${PWD##*/}" )
done

