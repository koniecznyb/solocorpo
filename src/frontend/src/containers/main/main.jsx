import React from 'react';
import { Route, Switch } from 'react-router-dom';
import About from '../../components/about';
import Dashboard from '../../components/dashboard';
import './main.css';
import InvoiceList from '../invoice/invoiceList';
import NewInvoice from '../invoice/newInvoice';

const Main = () => (
  <div className="panel">
    <Switch>
      <Route path="/dashboard" component={Dashboard} />
      <Route path="/invoices" component={InvoiceList} />
      <Route path="/about" component={About} />
      <Route path="/invoice/new" component={NewInvoice} />
    </Switch>
  </div>
);

export default Main;
