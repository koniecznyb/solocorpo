import React from 'react';
import { Formik } from 'formik';

const Signup = () => (
    <div className="signup">
        <Formik
            initialValues={{ email: '', password: '', vatNumber: '' }}
            validate={values => {
                const errors = {};
                if (!values.email) {
                    errors.email = 'Required!';
                } else if (
                    !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i.test(values.email)
                ) {
                    errors.email = 'Invalid email address';
                }

                if (!values.password) {
                    errors.password = 'Required!';
                }
                if (!values.vatNumber) {
                    errors.vatNumber = 'Required!';
                }
                return errors;
            }}
            onSubmit={(values, { setSubmitting }) => {
                setTimeout(() => {
                    alert(JSON.stringify(values, null, 2));
                    setSubmitting(false);
                }, 400);
            }}
        >
         {({
        values,
        errors,
        touched,
        handleChange,
        handleBlur,
        handleSubmit,
        isSubmitting,
        /* and other goodies */
      }) => (
        <form onSubmit={handleSubmit}>
        <label htmlFor="email">Email</label>
          <input
            type="email"
            name="email"
            onChange={handleChange}
            onBlur={handleBlur}
            value={values.email}
          />
          {errors.email && touched.email && errors.email}
          <label htmlFor="password">Password</label>
          <input
            type="password"
            name="password"
            onChange={handleChange}
            onBlur={handleBlur}
            value={values.password}
          />
          {errors.password && touched.password && errors.password}
          <label htmlFor="vatNumber">Vat number</label>
          <input
            type="vatNumber"
            name="vatNumber"
            onChange={handleChange}
            onBlur={handleBlur}
            value={values.vatNumber}
          />
          {errors.vatNumber && touched.vatNumber && errors.vatNumber}
          <button type="submit" disabled={isSubmitting}>
            Create
          </button>
        </form>
      )}
      </Formik>
  </div >
);

export default Signup;
