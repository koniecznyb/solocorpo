import { Cmd, loop } from 'redux-loop';
import base64 from 'base-64';
import { AUTHENTICATE, authenticateFailure, authenticateSuccess } from '../actions/actions';

const authenticateUser = (backendUrl, user, password) => fetch(
  `${backendUrl}/authenticate`, {
    method: 'POST',
    headers: { Authorization: `Basic ${base64.encode(`${user}:${password}`)}` },
    credentials: 'include',
  },
).then((response) => {
  if (response.status >= 400) {
    return Promise.reject(new Error('There was a problem with the request'));
  }
  if (response.status >= 300 && response.status < 400) {
    return Promise.reject(new Error('Incorrect authentication details'));
  }
  if (response.status >= 200) {
    return Promise.resolve(user);
  }
  return Promise.reject(new Error('resource unavailable'));
});

export default function reducer(state, action) {
  switch (action.type) {
    case AUTHENTICATE:
      return loop({
        ...state,
      },
      Cmd.run(
        authenticateUser, {
          successActionCreator: authenticateSuccess,
          failActionCreator: authenticateFailure,
          args: [
            state.backendUrl,
            action.user,
            action.password,
          ],
        },
      ));
    default:
      return state;
  }
}
