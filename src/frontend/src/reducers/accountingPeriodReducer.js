import { SELECT_ACCOUNTING_PERIOD } from '../actions/actions';

const [year, month] = new Date().toISOString().split('T')[0].split('-');
const initialState = {
  selectedDate: `${year}-${month}`,
};

export default function reducer(state = initialState, action) {
  switch (action.type) {
    case SELECT_ACCOUNTING_PERIOD:
      return {
        ...state,
        selectedDate: action.selectedDate,
      };
    default:
      return state;
  }
}
