import React from 'react';
import { Provider } from 'react-redux';
import PropTypes from 'prop-types';
import { Route, Router } from 'react-router';
import AppComponent from './containers/app/app';
import history from './constants/history';

const Root = ({ store }) => (
  <Provider store={store}>
    <Router history={history}>
      <Route path="/" component={AppComponent} />
    </Router>
  </Provider>
);

Root.propTypes = {
  store: PropTypes.shape({}).isRequired,
};

export default Root;
