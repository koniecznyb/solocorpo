#!/bin/bash

for d in src/*/
do
    ( cd "$d" && docker build . -t  "$REGISTRY/solocorpo-${PWD##*/}" )
done

