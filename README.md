### Intro

According to 12-factor app approach all the default configuration is production like with additional config files
for specific environments (development or staging).

Deployments are done using kubernetes clusters.

Defaults:
`docker-compose.yml`
For development:
`docker-compose-dev.yml`
Production config:
`docker-compose-prod.yml`

Kubernetes config files are generated from these using Kompose.

## Development with hot code reload

`cd docker`

Create local .env file with your example configuration

```
PROXY_HOST=localhost
FRONTEND_HOST=http://localhost
POSTGRES_USER=solocorpodb
POSTGRES_PASSWORD=q1w2e3r4
REGISTRY=registry.gitlab.com/koniecznyb/accounting
KAFKA_MANAGER_PASSWORD=q1w2e3r4
```

Build the images:

`docker-compose -f docker-compose.yml -f docker-compose-dev.yml build`

Start up all the dependencies:

`docker-compose -f docker-compose.yml -f docker-compose-dev.yml -f dev-linux-volumes.yml up -d kafka zookeeper sqldb redis`

Then, you can start the app with all the dependencies using docker command:

`docker-compose -f docker-compose.yml -f docker-compose-dev.yml -f dev-linux-volumes.yml up file-generation-service orchestration-service frontend file-persistence-service company-data-service nginx-proxy`


## Deployment
Deployment with scaled services:
`docker-compose -f docker-compose.yml -f docker-compose-dev.yml up --no-recreate --scale kafka=3 --scale orchestration-service=3 --scale file-generation-service=3`
